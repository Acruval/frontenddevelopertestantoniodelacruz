# Antonio CapSo frontend developer test task #

## Getting started ##

* Use bower and npm to install dependencies
* Run using `grunt serve`
* Go to localhost:9000 and further instructions will be provided there.

## About ##
* Read more in localhost:9000/#/about what is working.
* Nice to run with the cache of the browser disable.